================
licenses-formula
================

The licenses-formula is a support formula that collects license information about
the packages installed on a given machine and saves them into a designated location.

.. note::

    See the full `Salt Formulas installation and usage instructions
    <http://docs.saltstack.com/en/latest/topics/development/conventions/formulas.html>`_.

Available states
================

.. contents::
    :local:

``licenses``
------------

Retrieves the license information and saves it as `licenses-YYYY-MM-DD.csv` in the target location.

.. vim: fenc=utf-8 spell spl=en cc=100 tw=99 fo=want sts=4 sw=4 et